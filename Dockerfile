# FROM ppodgorsek/robot-framework
FROM python:3.9.18-bullseye

RUN apt-get update && apt-get install -y xvfb wget unzip libnss3-tools
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN apt-get update && apt-get install -y google-chrome-stable
RUN wget -q https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/119.0.6045.105/linux64/chromedriver-linux64.zip -O /tmp/chromedriver_linux64.zip \
    && unzip -qq /tmp/chromedriver_linux64.zip -d /usr/local/bin \
    && rm /tmp/chromedriver_linux64.zip

RUN pip install robotframework==6.1.1
RUN pip install robotframework-seleniumlibrary==6.1.3

COPY /script/run.sh /run.sh

CMD ["/run.sh"]
