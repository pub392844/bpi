*** Settings ***
Resource          ../resources/Keywords/ToDoList_Keyword.resource

Suite Setup         Open ToDo List web
Suite Teardown      Close Browser

*** Test Cases ***
Add item on [ADD ITEM Tab] verify result in [TO DO TASKS Tab]
    Verify Landing Page
    Add Item    aaa
    Add Item    bbb
    Add Item    ccc
    Add Item    ddd
    Verify Item "aaa" Added
    Verify Item "bbb" Added
    Verify Item "ccc" Added
    Verify Item "ddd" Added

Click title to set task as completed
    ${index}=    Verify Item "aaa" Added
    Click "${index}" title to complete task
    Verify that "aaa" dont exist in To-Do Tasks
    Verify COMPLETED Tab Contains "aaa"


Click check box to set task as completed
    ${index}=    Verify Item "bbb" Added
    Click "${index}" checkbox to complete task
    Verify that "bbb" dont exist in To-Do Tasks
    Verify COMPLETED Tab Contains "bbb"

Click delete to remove task on TO-DO TASKS tab and verify COMPLETED tab
    ${index}=    Verify Item "ccc" Added
    Click DELETE "${index}" task on TO-DO TASKS Tab
    Verify COMPLETED Tab NOT Contains "ccc"

Click delete to remove task on COMPLETED tab and verify COMPLETED tab
    Verify COMPLETED Tab Contains "aaa"
    Click DELETE "aaa" task on COMPLETED Tab
    Verify COMPLETED Tab NOT Contains "aaa"